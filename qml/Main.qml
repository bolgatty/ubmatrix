/*
 * Copyright (C) 2022  Gokul Kartha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubmatrix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Matrix 1.0

MainView {
    id: ubmatrix
    objectName: 'mainView'
    applicationName: 'ubmatrix.gokul'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    property bool initialised: false

    Connection { id: connection }
    Settings   { id: settings }

    function resync() {
        if(!initialised) {
            login.visible = false
            mainView.visible = true
            roomListItem.init()
            initialised = true
        }
        connection.sync(30000)
    }

    function login(user, pass, connect) {
        if(!connect) connect = connection.connectToServer

        connection.connected.connect(function() {
            settings.setValue("user",  connection.userId())
            settings.setValue("token", connection.token())

            connection.connectionError.connect(connection.reconnect)
            connection.syncDone.connect(resync)
            connection.reconnected.connect(resync)
            mainView.visible = true
            connection.sync()
        })


        var userParts = user.split(':')
        if(userParts.length === 1 || userParts[1] === "matrix.org") {
            connect(user, pass)
        } else {
            connection.resolved.connect(function() {
                connect(user, pass)
            })
            connection.resolveError.connect(function() {
                console.log("Couldn't resolve server!")
            })
            connection.resolveServer(userParts[1])
        }
    }

    Page {
        anchors.fill: parent

        Item {
            id: mainView
            anchors.fill: parent
            visible: false
            RowLayout {
                id: layout
                anchors.fill: parent
                spacing: 6
                RoomList {
                    id: roomListItem
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width / 3
                    Component.onCompleted: {
                        setConnection(connection)
                        enterRoom.connect(roomView.setRoom)
                        joinRoom.connect(connection.joinRoom)
                    }
                }

                RoomView {
                    id: roomView
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Component.onCompleted: {
                        setConnection(connection)
                    }
                }
            }
        }
        Login {
            id: login
            ubmatrix: ubmatrix
            anchors.fill: parent
            Component.onCompleted: {
                var user =  settings.value("user")
                var token = settings.value("token")
                if(user && token) {
                    ubmatrix.login(user, token, connection.connectWithToken)
                }
                connection.loginError.connect(function(error) {
                    console.log("here is a login error" +error)
                    //show an error
                    mainView.visible = false
                    login.visible=true
                    login.login_failed()
                    initialised = false
                })
            }
        }
    }
}
