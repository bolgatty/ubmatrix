#include "app_settings.h"

AppSettings::AppSettings(QObject *parent)
    : QSettings(parent) {
}

AppSettings::~AppSettings() {
}

void AppSettings::setValue(const QString &key, const QVariant &value) {
    QSettings::setValue(key, value);
}

QVariant AppSettings::value(const QString &key, const QVariant &defaultValue) const {
    return QSettings::value(key, defaultValue);
}
